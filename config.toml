# The URL the site will be built for
base_url = "https://papojari.codeberg.page"

# The site title and description; used in feeds by default.
title = "papojari"
description = "papojari's non-commercial website featuring a blog, games, an art portfolio, a link tree among others."

# The default language; used in feeds and search index
# Note: the search index doesn't support Chinese/Japanese/Korean Languages
default_language = "en"

# The site theme to use.
theme = "adidoks"

# Whether to automatically compile all Sass files in the sass directory
compile_sass = true

# Whether to generate a feed file for the site
generate_feed = true

# The filename to use for the feed. Used as the template filename, too.
# Defaults to "atom.xml", which has a built-in template that renders an Atom 1.0 feed.
# There is also a built-in template "rss.xml" that renders an RSS 2.0 feed.
feed_filename = "atom.xml"

# When set to "true", the generated HTML files are minified.
minify_html = false

# The taxonomies to be rendered for the site and their configuration.
taxonomies = [
  {name = "authors"}, # Basic definition: no feed or pagination
]

# Whether to build a search index to be used later on by a JavaScript library
# When set to "true", a search index is built from the pages and section
# content for `default_language`.
build_search_index = true

[search]
# Whether to include the title of the page/section in the index
include_title = true
# Whether to include the description of the page/section in the index
include_description = false
# Whether to include the rendered content of the page/section in the index
include_content = true

[markdown]
# Whether to do syntax highlighting.
# Theme can be customised by setting the `highlight_theme`
# variable to a theme supported by Zola
highlight_code = true

# When set to "true", emoji aliases translated to their corresponding
# Unicode emoji equivalent in the rendered Markdown files. (e.g.: :smile: => 😄)
render_emoji = true

# Whether smart punctuation is enabled (changing quotes, dashes, dots in their typographic form)
# For example, `...` into `…`, `"quote"` into `“curly”` etc
smart_punctuation = true

# Whether external links are to be opened in a new tab
# If this is true, a `rel="noopener"` will always automatically be added for security reasons
external_links_target_blank = true

[extra]
# Put all your custom variables here
author = "papojari"

# Set HTML file language
language_code = "en-US"

# Set theme-color meta tag for Chrome browser
theme_color = "#fff"

# More about site's title
title_separator = "|"  # set as |, -, _, etc
title_addition = "Modern Documentation Theme"

# Set date format in blog publish metadata
timeformat = "%Y-%M-%d %H:%m:%S" # e.g. 2021-19-01 09:05:42
timezone = "UTC"

# Math settings
# options: true, false. Enable math support globally,
# default: false. You can always enable math on a per page.
math = false
library = "katex"  # options: "katex", "mathjax". default is "katex".

# JSON-LD
[extra.schema]
type = "Person"
name = "papojari"
logo = "static/icon.svg"
url = "https://papojari.codeberg.page"
github = "https://github.com/papojari"
gitlab = "https://gitlab.com/papojari"
codeberg = "https://codeberg.org/papojari"
reddit = "https://reddit.com/veggushroom"
matrix = "https://matrix.to/#/@papojari:artemislena.eu"
mail = "mailto:papojari-git.ovoid@aleeas.com"
section = "blog" # see config.extra.main~url

# Sitelinks Search Box
site_links_search_box = false

# Menu items
[[extra.menu.main]]
name = "Blog"
section = "blog"
url = "/blog/"
weight = 20

[[extra.menu.social]]
name = "Keyoxide"
pre = '<img width="20" src="https://keyoxide.org/static/img/logo_circle.png"></img>'
url = "https://keyoxide.org/319A982F5F12013B730139FF5D98BEEC20C9695C"
post = "v0.1.0"
weight = 100

[[extra.menu.social]]
name = "GitHub"
pre = '<span class="iconify" data-icon="feather:github" data-width="20"></span>'
url = "https://github.com/papojari"
post = "v0.1.0"
weight = 10

[[extra.menu.social]]
name = "GitLab"
pre = '<span class="iconify" data-icon="feather:gitlab" data-width="20"></span>'
url = "https://gitlab.com/papojari"
post = "v0.1.0"
weight = 20

[[extra.menu.social]]
name = "Codeberg"
pre = '<span class="iconify" data-icon="simple-icons:codeberg" data-width="20"></span>'
url = "https://codeberg.org/papojari"
post = "v0.1.0"
weight = 20

[[extra.menu.social]]
name = "Reddit"
pre = '<span class="iconify" data-icon="ant-design:reddit-outlined" data-width="24"></span>'
url = "https://reddit.com/user/veggushroom"
weight = 10

[[extra.menu.social]]
name = "matrix"
pre = '<span class="iconify" data-icon="simple-icons:matrix" data-width="20"></span>'
url = "https://matrix.to/#/@papojari:artemislena.eu"
weight = 10

[[extra.menu.social]]
name = "mail"
pre = '<span class="iconify" data-icon="feather:mail" data-width="20"></span>'
url = "mailto:papojari-git.ovoid@aleeas.com"
weight = 10

# Footer contents
[extra.footer]
info = 'Powered by <a href="https://codeberg.page/">Codeberg Pages</a>, <a href="https://www.getzola.org/">Zola</a>, and <a href="https://github.com/papojari/adidoks">AdiDoks</a><a style="margin-left: 0.8em;" href="https://codeberg.org/papojari/zola-template">Source code</a><a style="margin-left: 0.8em;" href="https://codeberg.org/papojari/zola-template/issues">Report an issue</a>'

[[extra.footer.nav]]
name = "Atom feed"
url = "/atom.xml"
weight = 10

[[extra.footer.nav]]
name = "© License"
url = "/license/"
weight = 20