+++
title = 'This is a blog article for u'
date = 2021-12-17 17:57:13
+++

This is a example blog article. We got

# biggest heading

## big heading

### heading

#### small heading

##### smaller heading

###### smallest heading

🦏

and

😺

you can even do fresh tables

| fruits | vegetables |
|---|---|
| :apple: | :cucumber: |
| :banana: | :: |


here we have a *horitontal breaks* for **ya**

---

now onto some lists

## unordered

- amogus
- sus
- ![impasta](https://preview.redd.it/48i3kgu4bgr51.jpg?auto=webp&s=f655fdf70699d7e07ab7dc6e0cb3a19641876dcb)

## ordered

1. grab apple
2. eat apple
3. profit?

---

If you run this in a POSIX shell you will get a :rainbow:

```bash
cat /dev/random | base64 -w 0 | lolcat
```

> *imagine a quote here*