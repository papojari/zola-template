+++
title = '© License'
date = 2021-12-19 15:14:32
+++

The whole project including the adidoks git submodule is licensed under the [AdiDoks license](https://github.com/papojari/adidoks/blob/main/LICENSE.md).